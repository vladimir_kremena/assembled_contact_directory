package ua.kr.ex.acd.persistence.contact;

import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;

import java.util.List;
import java.util.Optional;

public interface ContactRepository {

    boolean save(Contact contact);

    List<Contact> getAll();

    Optional<Contact> findByValue(String value);

    List<Contact> findByStartValue(String startValue);

    List<Contact> findByPartName(String partName);

    List<Contact> findByType(ContactType contactType);

    boolean deleteByValue(String value);

}