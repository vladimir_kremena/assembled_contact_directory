package ua.kr.ex.acd.persistence.auth;

import ua.kr.ex.acd.model.Token;
import ua.kr.ex.acd.model.User;

public interface AuthRepository {

    boolean addUser(User user);

    User getUser(String login);

    Token getToken();

}