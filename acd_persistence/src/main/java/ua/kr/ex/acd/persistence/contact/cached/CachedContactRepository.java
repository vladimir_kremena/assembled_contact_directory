package ua.kr.ex.acd.persistence.contact.cached;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.persistence.contact.ContactRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class CachedContactRepository implements ContactRepository {

    private final ContactRepository contactRepository;

    private List<Contact> cache;

    @Override
    public boolean save(Contact contact) {
        invalidateCache();
        return contactRepository.save(contact);
    }

    @Override
    public List<Contact> getAll() {
        if (cache == null) {
            cache = contactRepository.getAll();
        }
        return cache;
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactRepository.findByValue(value);
    }

    @Override
    public List<Contact> findByStartValue(String startValue) {
        return contactRepository.findByStartValue(startValue);
    }

    @Override
    public List<Contact> findByPartName(String partName) {
        return contactRepository.findByPartName(partName);
    }

    @Override
    public List<Contact> findByType(ContactType contactType) {
        return contactRepository.findByType(contactType);
    }

    @Override
    public boolean deleteByValue(String value) {
        invalidateCache();
        return contactRepository.deleteByValue(value);
    }

    public void invalidateCache() {
        cache = null;
    }
}