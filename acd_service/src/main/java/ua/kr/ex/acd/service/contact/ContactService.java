package ua.kr.ex.acd.service.contact;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.exception.ContactNotFoundException;
import ua.kr.ex.acd.exception.DuplicateContactException;
import ua.kr.ex.acd.exception.ValidateException;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.persistence.contact.ContactRepository;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.util.validator.Validator;
import ua.kr.ex.acd.util.validator.contact.PhoneValidator;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ContactService {

    private final AuthService authService;
    private final ContactRepository contactRepository;
    private final Validator<Contact> contactValidator;

    public boolean save(Contact contact) {
        if (!contactValidator.isValid(contact)) {
            throw new ValidateException("Contact is not valid");
        }
        if (contact.getContactType() == ContactType.PHONE) {
            contact.setValue("+380" + new PhoneValidator().getBody(contact));
        }
        if (contactRepository.findByValue(contact.getValue()).isPresent() &&
                contactRepository.findByValue(contact.getValue()).get().getUserId()
                        .equals(authService.getAuthUser().getId())) {
            throw new DuplicateContactException("Contact already exists !!!");
        }
        contact.setUserId(authService.getAuthUser().getId());
        return contactRepository.save(contact);
    }

    public List<Contact> findAll() {
        return  contactRepository.getAll().stream()
                    .filter(contact -> contact.getUserId().equals(authService.getAuthUser().getId()))
                    .sorted(Contact.byFullNameContactTypeComparator())
                    .collect(Collectors.toList());
    }

    public List<Contact> findPhones() {
        return  contactRepository.findByType(ContactType.PHONE).stream()
                    .filter(contact -> contact.getUserId().equals(authService.getAuthUser().getId()))
                    .sorted(Contact.byFullNameContactTypeComparator())
                    .collect(Collectors.toList());
    }

    public List<Contact> findEmails() {
        return  contactRepository.findByType(ContactType.EMAIL).stream()
                    .filter(contact -> contact.getUserId().equals(authService.getAuthUser().getId()))
                    .sorted(Contact.byFullNameContactTypeComparator())
                    .collect(Collectors.toList());
    }

    public List<Contact> findByName(String namePart) {
        return  contactRepository.findByPartName(namePart).stream()
                    .filter(contact -> contact.getUserId().equals(authService.getAuthUser().getId()))
                    .sorted(Contact.byFullNameContactTypeComparator())
                    .collect(Collectors.toList());
    }

    public List<Contact> findByValueStart(String valueStart) {
        return  contactRepository.findByStartValue(valueStart).stream()
                    .filter(contact -> contact.getUserId().equals(authService.getAuthUser().getId()))
                    .sorted(Contact.byFullNameContactTypeComparator())
                    .collect(Collectors.toList());
    }

    public boolean deleteByValue(String value) {
        contactRepository.findByValue(value)
                .orElseThrow(() -> new ContactNotFoundException("Contact not found !!!"));
        return contactRepository.deleteByValue(value);
    }
}