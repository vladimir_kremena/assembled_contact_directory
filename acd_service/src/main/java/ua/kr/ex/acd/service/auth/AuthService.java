package ua.kr.ex.acd.service.auth;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.exception.AuthorizationException;
import ua.kr.ex.acd.exception.DuplicateUserException;
import ua.kr.ex.acd.model.Token;
import ua.kr.ex.acd.model.User;
import ua.kr.ex.acd.persistence.auth.AuthRepository;
import ua.kr.ex.acd.util.encoder.PasswordEncoder;

@RequiredArgsConstructor
public class AuthService {

    private final AuthRepository authRepository;
    private final PasswordEncoder passwordEncoder;
    @Getter
    private User authUser;
    @Getter
    private boolean auth;

    public boolean registration(User user) {
        if (authRepository.getUser(user.getLogin()) != null) {
            throw new DuplicateUserException("User already exists !!!");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return authRepository.addUser(user);
    }

    public boolean login(User user) {
        if (authUser != null && auth != false) {
            throw new AuthorizationException("Already logged in user");
        }
        authUser = authRepository.getUser(user.getLogin()) != null ?
                    authRepository.getUser(user.getLogin()) : new User();
        if (!user.getLogin().equals(authUser.getLogin()) ||
                !passwordEncoder.verify(user.getPassword(), authUser.getPassword())) {
            throw new IllegalArgumentException();
        }
        return auth = true;
    }

    public boolean logout() {
        auth = false;
        authUser = null;
        return true;
    }

    public Token getToken() {
        return authRepository.getToken();
    }
}