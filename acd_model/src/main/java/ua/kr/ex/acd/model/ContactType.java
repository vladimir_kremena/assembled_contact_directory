package ua.kr.ex.acd.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ContactType {

    PHONE("Phone number"), EMAIL("E-mail");

    @Getter
    private final String displayName;

}