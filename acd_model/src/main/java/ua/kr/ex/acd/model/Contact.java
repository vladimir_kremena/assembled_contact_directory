package ua.kr.ex.acd.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

@Getter @Setter
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contact implements Serializable {

    private String      id;
    @JsonProperty("type")
    private ContactType contactType;
    @JsonIgnore
    private String      firstName;
    @JsonIgnore
    private String      lastName;
    private String      value;
    private Long        userId;

    @JsonProperty("name")
    public String getFullName() {
        return lastName + " " + firstName;
    }

    @JsonProperty("name")
    public void setFullName(String name) {
        String[] names = name.split(" ");
        if (names.length > 0) {
            if (Objects.nonNull(names[0])) lastName = names[0];
            if (Objects.nonNull(names[1])) firstName = names[1];
        }
    }

    public static Comparator<Contact> byFullNameContactTypeComparator() {
        return (Contact current, Contact incoming) -> {
            if (current == null && incoming == null) return 0;
            if (current == null) return -1;
            if (incoming == null) return 1;
            int cmp = Objects.compare(current.lastName, incoming.lastName, String::compareTo);
            if (cmp == 0) {
                cmp = Objects.compare(current.firstName, incoming.firstName, String::compareTo);
            }
            if (cmp == 0) {
                cmp = Objects.compare(current.contactType,
                        incoming.contactType, ContactType::compareTo);
            }
            if (cmp == 0) {
                cmp = Objects.compare(current.value, incoming.value, String::compareTo);
            }
            return cmp;
        };
    }

    public static Comparator<Contact> byUserIdComparator() {
        return (Contact current, Contact incoming) -> {
            if (current == null && incoming == null) return 0;
            if (current == null) return -1;
            if (incoming == null) return 1;
            int cmp = Objects.compare(current.userId, incoming.userId, Long::compareTo);
            return cmp;
        };
    }

    @Override
    public String toString() {
        return  "Contact [" +
                "id = " + id + " | " +
                "contactType = " + contactType + " | " +
                "firstName = " + firstName + " | " +
                "lastName = " + lastName + " | " +
                "value = " + value + " | " +
                "userId = " + userId +
                "]";
    }

    public String viewForFile() {
        return  id + " | " +
                contactType + " | " +
                firstName + " | " +
                lastName + " | " +
                value + " | " +
                userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return  Objects.equals(id, contact.id) &&
                contactType == contact.contactType &&
                Objects.equals(firstName, contact.firstName) &&
                Objects.equals(lastName, contact.lastName) &&
                Objects.equals(value, contact.value) &&
                Objects.equals(userId, contact.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, contactType, firstName, lastName, value, userId);
    }
}