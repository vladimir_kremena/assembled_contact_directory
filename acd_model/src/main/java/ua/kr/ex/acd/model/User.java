package ua.kr.ex.acd.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

@Getter @Setter
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    private Long            id;
    private String          login;
    private String          password;
    private LocalDate       dateBorn;
    private LocalDateTime   dateRegistration;

    public static Comparator<User> byIdComparator() {
        return (current, incoming) -> {
            if (current == null && incoming == null) return 0;
            if (current == null) return -1;
            if (incoming == null) return 1;
            return Objects.compare(current.getId(), incoming.getId(), Long::compare);
        };
    }

    @Override
    public String toString() {
        return  "User [" +
                "id = " + id + " | " +
                "login = " + login + " | " +
                "password = " + password + " | " +
                "dateBorn = " + dateBorn + " | " +
                "dateRegistration = " + dateRegistration +
                "]";
    }
}