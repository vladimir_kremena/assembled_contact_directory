package ua.kr.ex.acd.model;

import java.time.LocalDateTime;

public class Token {

    private String          value;
    private LocalDateTime   createTime;
    private LocalDateTime   shutdownTime;
    private Boolean         valid;

    public Boolean isValid() {
        return valid;
    }
}