package ua.kr.ex.acd.ui;

import ua.kr.ex.acd.model.User;

public interface AuthView {

    User readUser();

    User createUser();

}