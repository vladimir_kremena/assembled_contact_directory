package ua.kr.ex.acd.ui;

import ua.kr.ex.acd.model.Contact;

import java.util.List;

public interface ContactView {

    Contact readContact();

    void showContacts(List<Contact> contactList);

    void pause();

    String readNamePart();

    String readStartContact();

    String readValue();

}