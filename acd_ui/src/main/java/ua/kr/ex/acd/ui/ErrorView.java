package ua.kr.ex.acd.ui;

public interface ErrorView {

    void showError(String errorMessage);

}