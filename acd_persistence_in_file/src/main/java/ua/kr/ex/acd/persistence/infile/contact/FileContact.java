package ua.kr.ex.acd.persistence.infile.contact;

import ua.kr.ex.acd.model.Contact;

import java.util.List;

public interface FileContact {

    List<Contact> readAll();

    void saveAll(List<Contact> contactList);

}