package ua.kr.ex.acd.persistence.infile.contact;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.persistence.contact.ContactRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class FileContactRepository implements ContactRepository {

    private final FileContact fileContact;

    @Override
    public boolean save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        List<Contact> contactList = getAll();
        contactList.add(contact);
        fileContact.saveAll(
                contactList.stream()
                        .sorted(Contact.byFullNameContactTypeComparator())
                        .sorted(Contact.byUserIdComparator())
                        .collect(Collectors.toList())
        );
        return true;
    }

    @Override
    public List<Contact> getAll() {
        return fileContact.readAll();
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return  getAll().stream()
                    .filter(contact -> contact.getValue().equals(value))
                    .findFirst();
    }

    @Override
    public List<Contact> findByStartValue(String startValue) {
        return  getAll().stream()
                    .filter(contact -> contact.getValue().startsWith(startValue))
                    .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByPartName(String partName) {
        return  getAll().stream()
                    .filter(contact -> contact.getFullName().contains(partName))
                    .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType contactType) {
        return  getAll().stream()
                    .filter(contact -> contact.getContactType() == contactType)
                    .collect(Collectors.toList());
    }

    @Override
    public boolean deleteByValue(String value) {
        fileContact.saveAll(
                getAll().stream()
                        .filter(contact -> !contact.getValue().equals(value))
                        .sorted(Contact.byFullNameContactTypeComparator())
                        .sorted(Contact.byUserIdComparator())
                        .collect(Collectors.toList())
        );
        return true;
    }
}