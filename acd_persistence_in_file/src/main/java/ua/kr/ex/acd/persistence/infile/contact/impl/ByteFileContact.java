package ua.kr.ex.acd.persistence.infile.contact.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.persistence.infile.contact.FileContact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ByteFileContact implements FileContact {

    private final String fileName;

    @Override
    public List<Contact> readAll() {
        List<Contact> contactList = new ArrayList<>();
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName))) {
            return contactList = (List<Contact>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            return contactList;
        }
    }

    @Override
    public void saveAll(List<Contact> contactList) {
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {
            objectOutputStream.writeObject(contactList);
            objectOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}