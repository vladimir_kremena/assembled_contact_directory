package ua.kr.ex.acd.persistence.infile.contact.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.persistence.infile.contact.FileContact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class TextFileContact implements FileContact {

    private final String fileName;

    @Override
    public List<Contact> readAll() {
        List<Contact> contactList = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while(true) {
                String stringContact = reader.readLine();
                if (stringContact == null) break;
                String[] contactField = stringContact.split(" \\| ");
                contactList.add(
                        new Contact()
                                .setId(contactField[0])
                                .setContactType(ContactType.valueOf(contactField[1]))
                                .setFirstName(contactField[2])
                                .setLastName(contactField[3])
                                .setValue(contactField[4])
                                .setUserId(Long.valueOf(contactField[5]))
                );
            }
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
        return contactList;
    }

    @Override
    public void saveAll(List<Contact> contactList) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (Contact contact : contactList) {
                writer.write(contact.viewForFile());
                writer.newLine();
                writer.flush();
            }
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
    }
}