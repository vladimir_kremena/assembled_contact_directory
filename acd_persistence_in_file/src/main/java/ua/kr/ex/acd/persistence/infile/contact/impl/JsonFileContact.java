package ua.kr.ex.acd.persistence.infile.contact.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.persistence.infile.contact.FileContact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class JsonFileContact implements FileContact {

    private final String fileName;
    private final ObjectMapper objectMapper;

    @Override
    public List<Contact> readAll() {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return objectMapper.readValue(bufferedReader, new TypeReference<ArrayList<Contact>>() {});
        } catch (IOException iOE) {
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> contactList) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(objectMapper.writeValueAsString(contactList));
            writer.flush();
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
    }
}