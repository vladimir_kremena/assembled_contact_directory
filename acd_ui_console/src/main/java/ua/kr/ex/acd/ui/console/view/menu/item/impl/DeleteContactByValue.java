package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.exception.ContactNotFoundException;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class DeleteContactByValue implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Delete by value";
    }

    @Override
    public void run() {
        try {
            contactService.deleteByValue(contactView.readValue());
        } catch (ContactNotFoundException e) {
            errorView.showError(e.getMessage());
        } catch (UnsupportedOperationException e) {
            errorView.showError("Unsupported Operation");
        }
    }
}