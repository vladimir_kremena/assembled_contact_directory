package ua.kr.ex.acd.ui.console.view;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.User;
import ua.kr.ex.acd.ui.AuthView;
import ua.kr.ex.acd.ui.console.view.util.PrintConsole;

import java.time.LocalDate;
import java.util.Scanner;

@RequiredArgsConstructor
public class ConsoleAuthView implements AuthView {

    private final Scanner scanner;

    @Override
    public User readUser() {
        PrintConsole.printRequestMessage("Enter login");
        String login = scanner.nextLine();
        PrintConsole.printRequestMessage("Enter password");
        String passwd = scanner.nextLine();
        return new  User()
                        .setLogin(login)
                        .setPassword(passwd);
    }

    @Override
    public User createUser() {
        PrintConsole.printRequestMessage("Enter login");
        String login = scanner.nextLine();
        PrintConsole.printRequestMessage("Enter password");
        String passwd = scanner.nextLine();
        PrintConsole.printRequestMessage("Enter year of born");
        int yearBorn;
        try {
            yearBorn = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nFE) {
            yearBorn = 1970;
        }
        PrintConsole.printRequestMessage("Enter month of born");
        int monthBorn;
        try {
            monthBorn = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nFE) {
            monthBorn = 1;
        }
        PrintConsole.printRequestMessage("Enter day of born");
        int dayBorn;
        try {
            dayBorn = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nFE) {
            dayBorn = 1;
        }
        return new User()
                        .setLogin(login)
                        .setPassword(passwd)
                        .setDateBorn(LocalDate.of(yearBorn, monthBorn, dayBorn));
    }
}