package ua.kr.ex.acd.ui.console.view;

import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.util.PrintConsole;

public class ConsoleErrorView implements ErrorView {

    @Override
    public void showError(String errorMessage) {
        PrintConsole.printDelimiter();
        PrintConsole.printMessage("!!!   Error   !!!");
        PrintConsole.printMessage(errorMessage);
        PrintConsole.printDelimiter();
    }
}