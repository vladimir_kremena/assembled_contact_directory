package ua.kr.ex.acd.ui.console.view.menu.item;

public interface MenuItem {

    String getName();

    void run();

    default boolean isFinal() {
        return false;
    }
}