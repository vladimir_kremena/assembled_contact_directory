package ua.kr.ex.acd.ui.console.view;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.View;
import ua.kr.ex.acd.ui.console.view.menu.Menu;
import ua.kr.ex.acd.ui.console.view.menu.item.creator.CreatorMenuItems;

import java.util.Scanner;

@RequiredArgsConstructor
public class ConsoleView implements View {

    private final CreatorMenuItems creatorMenuItems;
    private final AuthService authService;
    private final ErrorView errorView;
    private final Scanner scanner;

    @Override
    public void show() {
        Menu menu = new Menu(creatorMenuItems, authService, errorView, scanner);
        menu.run();
    }
}