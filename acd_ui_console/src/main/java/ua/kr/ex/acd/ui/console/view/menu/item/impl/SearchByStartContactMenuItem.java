package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class SearchByStartContactMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Search by start of contact";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findByValueStart(contactView.readStartContact()));
        contactView.pause();
    }
}