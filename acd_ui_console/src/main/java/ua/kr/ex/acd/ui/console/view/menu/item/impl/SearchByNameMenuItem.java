package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class SearchByNameMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Show matches by name";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findByName(contactView.readNamePart()));
        contactView.pause();
    }
}