package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class ShowOnlyEmailMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Show only emails";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findEmails());
        contactView.pause();
    }
}