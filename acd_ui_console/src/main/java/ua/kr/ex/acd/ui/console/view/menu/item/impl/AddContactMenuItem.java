package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.exception.DuplicateContactException;
import ua.kr.ex.acd.exception.ValidateException;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class AddContactMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Add contact";
    }

    @Override
    public void run() {
        try {
            Contact contact = contactView.readContact();
            contactService.save(contact);
        } catch (DuplicateContactException | ValidateException dCE) {
            errorView.showError(dCE.getMessage());
        }
    }
}