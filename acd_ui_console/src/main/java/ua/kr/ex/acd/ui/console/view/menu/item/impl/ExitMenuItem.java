package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;
import ua.kr.ex.acd.ui.console.view.util.PrintConsole;

@RequiredArgsConstructor
public class ExitMenuItem implements MenuItem {

    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void run() {
        PrintConsole.printMessage("Good by...");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}