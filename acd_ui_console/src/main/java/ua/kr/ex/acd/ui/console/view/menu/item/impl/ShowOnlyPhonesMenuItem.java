package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class ShowOnlyPhonesMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Show only phones";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findPhones());
        contactView.pause();
    }
}