package ua.kr.ex.acd.ui.console.view.menu.item.creator;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.AuthView;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;
import ua.kr.ex.acd.ui.console.view.menu.item.impl.*;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class CreatorMenuItems {

    private final AuthView authView;
    private final ErrorView errorView;
    private final AuthService authService;
    private final ContactService contactService;
    private final ContactView contactView;

    public List<MenuItem> getMenuItems(boolean auth) {

        Map<Boolean, List<MenuItem>> factories = Map.of(
                false, List.of(
                        new LoginMenuItem(authService, authView, errorView),
                        new RegistrationMenuItem(authService, authView, errorView),
                        new ExitMenuItem()),
                true, List.of(
                        new AddContactMenuItem(contactService, contactView, errorView),
                        new ShowAllMenuItem(contactService, contactView),
                        new ShowOnlyPhonesMenuItem(contactService, contactView),
                        new ShowOnlyEmailMenuItem(contactService, contactView),
                        new SearchByNameMenuItem(contactService, contactView),
                        new SearchByStartContactMenuItem(contactService, contactView),
                        new DeleteContactByValue(contactService, contactView, errorView),
                        new LogoutMenuItem(authService, errorView),
                        new ExitMenuItem()
                )
        );
        return factories.get(auth);
    }
}