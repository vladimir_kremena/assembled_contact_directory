package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.ui.AuthView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class LoginMenuItem implements MenuItem {

    private final AuthService authService;
    private final AuthView authView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "LogIn";
    }

    @Override
    public void run() {
        try {
            authService.login(authView.readUser());
        } catch (Exception iAE) {
            errorView.showError("Incorrect login or password!!!");
        }
    }
}