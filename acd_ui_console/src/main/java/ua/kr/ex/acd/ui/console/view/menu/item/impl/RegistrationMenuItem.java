package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.exception.DuplicateUserException;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.ui.AuthView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

@RequiredArgsConstructor
public class RegistrationMenuItem implements MenuItem {

    private final AuthService authService;
    private final AuthView authView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Registration";
    }

    @Override
    public void run() {
        try {
            authService.registration(authView.createUser());
        } catch (DuplicateUserException dUE) {
            errorView.showError(dUE.getMessage());
        } catch (RuntimeException e) {
            errorView.showError("Failed to add user");
        }
    }
}