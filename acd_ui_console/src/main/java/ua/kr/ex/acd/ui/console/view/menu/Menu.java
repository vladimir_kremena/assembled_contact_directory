package ua.kr.ex.acd.ui.console.view.menu;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;
import ua.kr.ex.acd.ui.console.view.menu.item.creator.CreatorMenuItems;
import ua.kr.ex.acd.ui.console.view.util.PrintConsole;

import java.util.List;
import java.util.Scanner;

@RequiredArgsConstructor
public class Menu {

    private List<MenuItem> items;

    private final CreatorMenuItems creatorMenuItems;
    private final AuthService authService;
    private final ErrorView errorView;
    private final Scanner scanner;

    public void run() {

        while(true) {
            items = creatorMenuItems.getMenuItems(authService.isAuth());
            showMenu();
            int choice = getChoiceUser();
            if(choice >= 0 && choice < items.size()) {
                MenuItem item = items.get(choice);
                item.run();
                if (item.isFinal()) {
                    return;
                }
            } else {
                errorView.showError("Invalid choice, try again ... ");
            }
        }
    }

    private void showMenu() {
        PrintConsole.printDelimiter();
        PrintConsole.printMessage("Contact directory");
        PrintConsole.printMessage("Menu");
        PrintConsole.printMenu(items);
        PrintConsole.printInnerDelimiter();
    }

    private int getChoiceUser() {
        PrintConsole.printRequestMessage("Enter your choice");
        int choice = 0;
        try {
            choice = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nFE) {}
        return choice - 1;
    }
}