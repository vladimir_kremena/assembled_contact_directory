package ua.kr.ex.acd.ui.console.view;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.util.PrintConsole;

import java.util.List;
import java.util.Scanner;

@RequiredArgsConstructor
public class ConsoleContactView implements ContactView {

    private final Scanner scanner;
    private final ErrorView errorView;

    @Override
    public Contact readContact() {
        PrintConsole.printRequestMessage("Enter contact first name");
        String firstName = scanner.nextLine();
        PrintConsole.printRequestMessage("Enter contact last name");
        String lastName = scanner.nextLine();
        ContactType type = readContactType();
        PrintConsole.printRequestMessage("Enter contact value");
        String value = scanner.nextLine();
        return new Contact()
                        .setFirstName(firstName)
                        .setLastName(lastName)
                        .setContactType(type)
                        .setValue(value);
    }

    private ContactType readContactType() {
        ContactType[] types = ContactType.values();
        boolean exit = false;
        int choice = 0;
        while(!exit) {
            PrintConsole.printMenuContactType(types);
            try {
                choice = getChoice();
                if (choice < 0 || choice >= types.length) {
                    errorView.showError("Invalid choice, try again ... ");
                    continue;
                }
            } catch (NumberFormatException nFE) {
                errorView.showError("Invalid choice, try again ... ");
                continue;
            }
            exit = true;
        }
        return types[choice];
    }

    private int getChoice() {
        PrintConsole.printRequestMessage("Enter your choice");
        int choice = Integer.parseInt(scanner.nextLine());
        return choice - 1;
    }

    @Override
    public void pause() {
        PrintConsole.printRequestMessage("Press enter");
        scanner.nextLine();
    }

    @Override
    public void showContacts(List<Contact> contactList) {
        PrintConsole.printDelimiter();
        System.out.printf("|%36s|%14s|%30s|%35s|\n", "Id", "Type", "Name", "Value");
        System.out.println("|" + "-".repeat(118) + "|");
        for (Contact contact : contactList) {
            System.out.printf("|%36s|%14s|%30s|%35s|\n",
                    contact.getId(),
                    contact.getContactType().getDisplayName(),
                    contact.getFullName(),
                    contact.getValue());
        }
        System.out.println("|" + "-".repeat(118) + "|");
    }

    @Override
    public String readNamePart() {
        PrintConsole.printInnerDelimiter();
        PrintConsole.printRequestMessage("Enter a name or part of a name");
        String namePart = scanner.nextLine();
        return namePart;
    }

    public String readStartContact() {
        PrintConsole.printInnerDelimiter();
        PrintConsole.printRequestMessage("Enter start of contact");
        String valueStart = scanner.nextLine();
        return valueStart;
    }

    @Override
    public String readValue() {
        PrintConsole.printRequestMessage("Enter contact value");
        String value = scanner.nextLine();
        return value;
    }
}