package ua.kr.ex.acd.ui.console.view.util;

import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;

import java.util.List;

public class PrintConsole {

    public static void printMenu(List<MenuItem> items) {
        for (int index = 0; index < items.size(); index++) {
            System.out.printf("|   %2d - %s\n", index + 1, items.get(index).getName() +
                    " ".repeat(120 - 10 - items.get(index).getName().length()) + "|");
        }
    }

    public static void printMenuContactType(ContactType[] types) {
        printInnerDelimiter();
        printInnerMessage("Select contact type : ");
        System.out.println();
        for (int index = 0; index < types.length; index++) {
            System.out.printf("|   %2d - %s\n", index + 1, types[index].getDisplayName() +
                    " ".repeat(120 - 10 - types[index].getDisplayName().length()) + "|");
        }
    }

    public static void printDelimiter() {
        System.out.println("|" + "-".repeat(118) + "|");
    }

    public static void printInnerDelimiter() {
        System.out.println("|    " + "-".repeat(110) + "    |");
    }

    public static void printMessage(String message) {
        if (message.length() % 2 != 0) {
            message = message.concat(" ");
        }
        System.out.println("|" + "-".repeat(60 - 4 - message.length() / 2) + "   " + message + "   " +
                "-".repeat(60 - 4 - message.length() / 2) + "|");
    }

    public static void printInnerMessage(String message) {
        System.out.print("|    " + message + " ".repeat(120 - 10 - message.length()) + "    |");
    }

    public static void printRequestMessage(String message) {
        System.out.print("|    " + message + " : ");
    }
}