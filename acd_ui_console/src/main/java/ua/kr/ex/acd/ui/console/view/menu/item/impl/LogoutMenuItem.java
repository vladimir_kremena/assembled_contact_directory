package ua.kr.ex.acd.ui.console.view.menu.item.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.console.view.menu.item.MenuItem;
import ua.kr.ex.acd.ui.console.view.util.PrintConsole;

@RequiredArgsConstructor
public class LogoutMenuItem implements MenuItem {

    private final AuthService authService;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "LogOut";
    }

    @Override
    public void run() {
        try {
            authService.logout();
            PrintConsole.printMessage("Logout... ");
        } catch (UnsupportedOperationException uoE) {
            errorView.showError("Unsupported Operation");
        }
    }
}