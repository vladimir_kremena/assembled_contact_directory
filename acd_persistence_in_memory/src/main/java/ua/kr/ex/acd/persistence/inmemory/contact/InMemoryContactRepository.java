package ua.kr.ex.acd.persistence.inmemory.contact;

import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.persistence.contact.ContactRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class InMemoryContactRepository implements ContactRepository {

    private List<Contact> contactList = new ArrayList<>();

    @Override
    public boolean save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        return contactList.add(contact);
    }

    @Override
    public List<Contact> getAll() {
        return contactList;
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return  contactList.stream()
                    .filter(contact -> contact.getValue().equals(value))
                    .findFirst();
    }

    @Override
    public List<Contact> findByStartValue(String startValue) {
        return  contactList.stream()
                    .filter(contact -> contact.getValue().startsWith(startValue))
                    .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByPartName(String partName) {
        return  contactList.stream()
                    .filter(contact -> contact.getFullName().contains(partName))
                    .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType contactType) {
        return  contactList.stream()
                    .filter(contact -> contact.getContactType() == contactType)
                    .collect(Collectors.toList());
    }

    @Override
    public boolean deleteByValue(String value) {
        contactList = contactList.stream()
                    .filter(contact -> !contact.getValue().equals(value))
                    .collect(Collectors.toList());
        return true;
    }
}