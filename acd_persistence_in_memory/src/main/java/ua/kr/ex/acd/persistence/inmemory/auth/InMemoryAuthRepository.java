package ua.kr.ex.acd.persistence.inmemory.auth;

import ua.kr.ex.acd.model.Token;
import ua.kr.ex.acd.model.User;
import ua.kr.ex.acd.persistence.auth.AuthRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class InMemoryAuthRepository implements AuthRepository {

    private List<User> userList = new ArrayList<>();

    @Override
    public boolean addUser(User user) {
        Long id = userList.stream().max(User.byIdComparator()).isPresent() ?
                    userList.stream().max(User.byIdComparator()).get().getId() + 1 : 1;
        user    .setId(id)
                .setDateRegistration(LocalDateTime.now());
        return userList.add(user);
    }

    @Override
    public User getUser(String login) {
        return  userList.stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Token getToken() {
        throw new UnsupportedOperationException();
    }
}