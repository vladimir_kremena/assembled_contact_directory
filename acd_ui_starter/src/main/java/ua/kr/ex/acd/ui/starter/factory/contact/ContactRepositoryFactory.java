package ua.kr.ex.acd.ui.starter.factory.contact;

import ua.kr.ex.acd.persistence.contact.ContactRepository;

public interface ContactRepositoryFactory {

    ContactRepository createContactRepository();

}