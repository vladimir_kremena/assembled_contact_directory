package ua.kr.ex.acd.ui.starter.factory.view.impl.creator;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.starter.factory.view.ViewFactory;
import ua.kr.ex.acd.ui.starter.factory.view.impl.ConsoleViewFactory;

import java.util.Map;

@RequiredArgsConstructor
public class CreatorViewFactory {

    private final AuthService authService;
    private final ContactService contactService;

    public ViewFactory createViewFactory(String applicationMode) {
        Map<String, ViewFactory> factories =
                Map.of(
                        "CONSOLE_MODE", new ConsoleViewFactory(authService, contactService)
                );
        if (!factories.containsKey(applicationMode)) {
            throw new RuntimeException("Invalid mode configuration");
        }
        return factories.get(applicationMode);
    }
}