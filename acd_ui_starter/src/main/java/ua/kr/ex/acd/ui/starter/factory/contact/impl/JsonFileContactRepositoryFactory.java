package ua.kr.ex.acd.ui.starter.factory.contact.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.persistence.contact.ContactRepository;
import ua.kr.ex.acd.persistence.contact.cached.CachedContactRepository;
import ua.kr.ex.acd.persistence.infile.contact.FileContactRepository;
import ua.kr.ex.acd.persistence.infile.contact.impl.JsonFileContact;
import ua.kr.ex.acd.ui.starter.factory.contact.ContactRepositoryFactory;

@RequiredArgsConstructor
public class JsonFileContactRepositoryFactory implements ContactRepositoryFactory {

    private final String fileName;
    private final ObjectMapper objectMapper;

    @Override
    public ContactRepository createContactRepository() {

        return new CachedContactRepository(
                new FileContactRepository(
                        new JsonFileContact(fileName, objectMapper)
                )
        );
    }
}