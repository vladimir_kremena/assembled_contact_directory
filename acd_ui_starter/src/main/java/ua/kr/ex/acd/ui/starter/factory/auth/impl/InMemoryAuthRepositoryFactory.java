package ua.kr.ex.acd.ui.starter.factory.auth.impl;

import ua.kr.ex.acd.persistence.auth.AuthRepository;
import ua.kr.ex.acd.persistence.inmemory.auth.InMemoryAuthRepository;
import ua.kr.ex.acd.ui.starter.factory.auth.AuthRepositoryFactory;

public class InMemoryAuthRepositoryFactory implements AuthRepositoryFactory {

    @Override
    public AuthRepository createAuthRepository() {
        return new InMemoryAuthRepository();
    }
}