package ua.kr.ex.acd.ui.starter.factory.contact.impl.creator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.ui.starter.factory.contact.ContactRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.contact.impl.ByteFileContactRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.contact.impl.InMemoryContactRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.contact.impl.JsonFileContactRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.contact.impl.TextFileContactRepositoryFactory;
import ua.kr.ex.acd.util.file.FileUtil;

import java.io.File;
import java.util.Map;

@RequiredArgsConstructor
public class CreatorContactRepositoryFactory {

    public ContactRepositoryFactory getContactRepositoryFactory(
            String storageMode, String fileName, ObjectMapper objectMapper) {
        File byteFile = new File(fileName);
        if (!byteFile.exists()) {
            FileUtil.createDirectoryAndFile(byteFile);
        }
        Map<String, ContactRepositoryFactory> factories =
                Map.of(
                        "IN_MEMORY", new InMemoryContactRepositoryFactory(),
                        "BYTE_FILE", new ByteFileContactRepositoryFactory(fileName),
                        "TEXT_FILE", new TextFileContactRepositoryFactory(fileName),
                        "JSON_FILE", new JsonFileContactRepositoryFactory(fileName, objectMapper)
                );
        if (!factories.containsKey(storageMode)) {
            throw new RuntimeException("Invalid mode configuration");
        }
        return factories.get(storageMode);
    }
}