package ua.kr.ex.acd.ui.starter.factory.auth;

import ua.kr.ex.acd.persistence.auth.AuthRepository;

public interface AuthRepositoryFactory {

    AuthRepository createAuthRepository();

}