package ua.kr.ex.acd.ui.starter.factory.auth.impl.creator;

import ua.kr.ex.acd.ui.starter.factory.auth.AuthRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.auth.impl.InMemoryAuthRepositoryFactory;

import java.util.Map;

public class CreatorAuthRepositoryFactory {

    public AuthRepositoryFactory getAuthRepositoryFactory(String storageMode) {
        Map<String, AuthRepositoryFactory> factories =
                Map.of(
                        "IN_MEMORY", new InMemoryAuthRepositoryFactory(),
                        "BYTE_FILE", new InMemoryAuthRepositoryFactory(),
                        "TEXT_FILE", new InMemoryAuthRepositoryFactory(),
                        "JSON_FILE", new InMemoryAuthRepositoryFactory()
                );
        if (!factories.containsKey(storageMode)) {
            throw new RuntimeException("Invalid mode configuration");
        }
        return factories.get(storageMode);
    }
}