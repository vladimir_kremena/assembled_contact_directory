package ua.kr.ex.acd.ui.starter.factory.contact.impl;

import ua.kr.ex.acd.persistence.contact.ContactRepository;
import ua.kr.ex.acd.persistence.contact.cached.CachedContactRepository;
import ua.kr.ex.acd.persistence.inmemory.contact.InMemoryContactRepository;
import ua.kr.ex.acd.ui.starter.factory.contact.ContactRepositoryFactory;

public class InMemoryContactRepositoryFactory implements ContactRepositoryFactory {

    @Override
    public ContactRepository createContactRepository() {
        return new CachedContactRepository(new InMemoryContactRepository());
    }
}