package ua.kr.ex.acd.ui.starter.factory.view;

import ua.kr.ex.acd.ui.View;

public interface ViewFactory {

    View createView();

}