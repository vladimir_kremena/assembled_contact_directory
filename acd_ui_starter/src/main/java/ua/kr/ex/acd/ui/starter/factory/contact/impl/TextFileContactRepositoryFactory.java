package ua.kr.ex.acd.ui.starter.factory.contact.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.persistence.contact.ContactRepository;
import ua.kr.ex.acd.persistence.contact.cached.CachedContactRepository;
import ua.kr.ex.acd.persistence.infile.contact.FileContactRepository;
import ua.kr.ex.acd.persistence.infile.contact.impl.TextFileContact;
import ua.kr.ex.acd.ui.starter.factory.contact.ContactRepositoryFactory;

@RequiredArgsConstructor
public class TextFileContactRepositoryFactory implements ContactRepositoryFactory {

    private final String fileName;

    @Override
    public ContactRepository createContactRepository() {

        return new CachedContactRepository(
                new FileContactRepository(
                        new TextFileContact(fileName)
                )
        );
    }
}