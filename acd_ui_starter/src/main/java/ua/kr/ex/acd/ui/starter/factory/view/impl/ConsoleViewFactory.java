package ua.kr.ex.acd.ui.starter.factory.view.impl;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.ContactView;
import ua.kr.ex.acd.ui.ErrorView;
import ua.kr.ex.acd.ui.View;
import ua.kr.ex.acd.ui.console.view.ConsoleAuthView;
import ua.kr.ex.acd.ui.console.view.ConsoleContactView;
import ua.kr.ex.acd.ui.console.view.ConsoleErrorView;
import ua.kr.ex.acd.ui.console.view.ConsoleView;
import ua.kr.ex.acd.ui.console.view.menu.item.creator.CreatorMenuItems;
import ua.kr.ex.acd.ui.starter.factory.view.ViewFactory;

import java.util.Scanner;

@RequiredArgsConstructor
public class ConsoleViewFactory implements ViewFactory {

    private final AuthService authService;
    private final ContactService contactService;

    @Override
    public View createView() {

        Scanner scanner = new Scanner(System.in);
        ErrorView errorView = new ConsoleErrorView();
        ContactView contactView = new ConsoleContactView(scanner, errorView);
        CreatorMenuItems creatorMenuItems = new CreatorMenuItems(
                new ConsoleAuthView(scanner),
                errorView,
                authService,
                contactService,
                contactView
        );
        return new ConsoleView(
                creatorMenuItems,
                authService,
                errorView,
                scanner);
    }
}