package ua.kr.ex.acd.ui.starter;

import com.fasterxml.jackson.databind.ObjectMapper;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.persistence.auth.AuthRepository;
import ua.kr.ex.acd.persistence.contact.ContactRepository;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.View;
import ua.kr.ex.acd.ui.starter.factory.auth.impl.creator.CreatorAuthRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.contact.impl.creator.CreatorContactRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.view.impl.creator.CreatorViewFactory;
import ua.kr.ex.acd.util.encoder.PasswordEncoder;
import ua.kr.ex.acd.util.encoder.impl.MD5PasswordEncoder;
import ua.kr.ex.acd.util.property.ApplicationPropertiesReader;
import ua.kr.ex.acd.util.validator.Validator;
import ua.kr.ex.acd.util.validator.contact.ContactValidator;
import ua.kr.ex.acd.util.validator.contact.EmailValidator;
import ua.kr.ex.acd.util.validator.contact.PhoneValidator;

import java.util.List;

public class Starter {

    public static final String APPLICATION_MODE =
            ApplicationPropertiesReader.getApplicationProperties().getProperty("application.mode");
    public static final String STORAGE_MODE =
            ApplicationPropertiesReader.getApplicationProperties().getProperty("storage.mode");
    public static final String FILE_NAME =
            ApplicationPropertiesReader.getApplicationProperties().getProperty("file.name");

    public static void main(String[] args) {

        Validator<Contact> contactValidator = new ContactValidator(
                List.of(
                        new PhoneValidator(),
                        new EmailValidator()
                )
        );
        ObjectMapper objectMapper = new ObjectMapper();
        PasswordEncoder passwordEncoder = new MD5PasswordEncoder();

        AuthRepository authRepository =
                new CreatorAuthRepositoryFactory()
                        .getAuthRepositoryFactory(STORAGE_MODE)
                        .createAuthRepository();

        AuthService authService = new AuthService(authRepository, passwordEncoder);

        ContactRepository contactRepository =
                new CreatorContactRepositoryFactory()
                        .getContactRepositoryFactory(STORAGE_MODE, FILE_NAME, objectMapper)
                        .createContactRepository();

        ContactService contactService = new ContactService(authService, contactRepository, contactValidator);

        View view = new CreatorViewFactory(authService, contactService)
                .createViewFactory(APPLICATION_MODE)
                .createView();
        view.show();
    }
}