package ua.kr.ex.acd.service.auth.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kr.ex.acd.exception.DuplicateUserException;
import ua.kr.ex.acd.model.User;
import ua.kr.ex.acd.persistence.auth.AuthRepository;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.ui.starter.factory.auth.impl.creator.CreatorAuthRepositoryFactory;
import ua.kr.ex.acd.util.encoder.PasswordEncoder;
import ua.kr.ex.acd.util.encoder.impl.MD5PasswordEncoder;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryAuthTest {

    private final String STORAGE_MODE = "IN_MEMORY";
    private final PasswordEncoder passwordEncoder = new MD5PasswordEncoder();

    private final AuthRepository authRepository = new CreatorAuthRepositoryFactory()
                    .getAuthRepositoryFactory(STORAGE_MODE)
                    .createAuthRepository();

    private final AuthService authService = new AuthService(authRepository, passwordEncoder);

    private final User registeredAdmin = new User()
            .setLogin("admin")
            .setPassword("admin")
            .setDateBorn(LocalDate.of(1970, 1, 1));
    private final User authorizedAdmin = new User()
            .setLogin("admin")
            .setPassword("admin");

    private final User registeredVladimir = new User()
            .setLogin("vladimir")
            .setPassword("vladimir")
            .setDateBorn(LocalDate.of(1986, 8, 25))
            .setDateRegistration(LocalDateTime.now());
    private final User authorizedVladimir = new User()
            .setLogin("vladimir")
            .setPassword("vladimir");

    @Test
    @DisplayName("in memory auth test")
    void allInMemoryAuthTest() {
        assertAll(
                () -> assertTrue(authService.registration(registeredAdmin)),
                () -> assertTrue(authService.registration(registeredVladimir)),
                () -> assertThrows(DuplicateUserException.class, () -> authService.registration(registeredAdmin)),
                () -> assertThrows(DuplicateUserException.class, () -> authService.registration(registeredVladimir)),

                () -> assertTrue(authService.login(authorizedAdmin)),
                () -> assertTrue(authService.isAuth()),
                () -> assertTrue(authService.getAuthUser().getLogin().equals(authorizedAdmin.getLogin()) &&
                        authService.getAuthUser().getPassword().equals(passwordEncoder.encode(authorizedAdmin.getPassword()))),
                () -> assertThrows(UnsupportedOperationException.class, () -> authService.getToken()),
                () -> assertTrue(authService.logout()),
                () -> assertFalse(authService.isAuth()),
                () -> assertEquals(authService.getAuthUser(), null),

                () -> assertTrue(authService.login(authorizedVladimir)),
                () -> assertTrue(authService.isAuth()),
                () -> assertTrue(authService.getAuthUser().getLogin().equals(authorizedVladimir.getLogin()) &&
                        authService.getAuthUser().getPassword().equals(passwordEncoder.encode(authorizedVladimir.getPassword()))),
                () -> assertThrows(UnsupportedOperationException.class, () -> authService.getToken()),
                () -> assertTrue(authService.logout()),
                () -> assertFalse(authService.isAuth()),
                () -> assertEquals(authService.getAuthUser(), null)
        );
    }
}