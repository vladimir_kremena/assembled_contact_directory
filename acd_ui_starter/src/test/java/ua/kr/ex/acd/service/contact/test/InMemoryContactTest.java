package ua.kr.ex.acd.service.contact.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kr.ex.acd.exception.DuplicateContactException;
import ua.kr.ex.acd.exception.ValidateException;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.model.User;
import ua.kr.ex.acd.persistence.auth.AuthRepository;
import ua.kr.ex.acd.persistence.contact.ContactRepository;
import ua.kr.ex.acd.service.auth.AuthService;
import ua.kr.ex.acd.service.contact.ContactService;
import ua.kr.ex.acd.ui.starter.factory.auth.impl.creator.CreatorAuthRepositoryFactory;
import ua.kr.ex.acd.ui.starter.factory.contact.impl.creator.CreatorContactRepositoryFactory;
import ua.kr.ex.acd.util.encoder.PasswordEncoder;
import ua.kr.ex.acd.util.encoder.impl.MD5PasswordEncoder;
import ua.kr.ex.acd.util.validator.Validator;
import ua.kr.ex.acd.util.validator.contact.ContactValidator;
import ua.kr.ex.acd.util.validator.contact.EmailValidator;
import ua.kr.ex.acd.util.validator.contact.PhoneValidator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryContactTest {

    private final String STORAGE_MODE = "IN_MEMORY";
    private final PasswordEncoder passwordEncoder = new MD5PasswordEncoder();

    private final Validator<Contact> contactValidator = new ContactValidator(
            List.of(
                    new PhoneValidator(),
                    new EmailValidator()
            )
    );

    private final AuthRepository authRepository = new CreatorAuthRepositoryFactory()
            .getAuthRepositoryFactory(STORAGE_MODE)
            .createAuthRepository();

    private final AuthService authService = new AuthService(authRepository, passwordEncoder);

    private final ContactRepository contactRepository =
            new CreatorContactRepositoryFactory()
                    .getContactRepositoryFactory(STORAGE_MODE, null, null)
                    .createContactRepository();

    private final ContactService contactService = new ContactService(authService, contactRepository, contactValidator);

    private final User registeredAdmin = new User()
            .setLogin("admin")
            .setPassword("admin")
            .setDateBorn(LocalDate.of(1970, 1, 1));
    private final User authorizedAdmin = new User()
            .setLogin("admin")
            .setPassword("admin");

    private final User registeredVladimir = new User()
            .setLogin("vladimir")
            .setPassword("vladimir")
            .setDateBorn(LocalDate.of(1986, 8, 25))
            .setDateRegistration(LocalDateTime.now());
    private final User authorizedVladimir = new User()
            .setLogin("vladimir")
            .setPassword("vladimir");

    @Test
    @DisplayName("in memory contact test")
    void allInMemoryContactTest() {

        assertAll(
                () -> assertTrue(authService.registration(registeredAdmin)),
                () -> assertTrue(authService.registration(registeredVladimir)),

                () -> assertTrue(authService.login(authorizedAdmin)),
                () -> assertEquals(authService.getAuthUser().getLogin(), authorizedAdmin.getLogin()),

                () -> assertEquals((long) contactService.findAll().size(), 0),

                () -> assertThrows(ValidateException.class, () -> contactService.save(invalidatePhoneContact)),
                () -> assertThrows(ValidateException.class, () -> contactService.save(invalidateEmailContact)),

                () -> adminContactList.forEach(contactService::save),
                () -> assertThrows(DuplicateContactException.class, () -> contactService.save(adminContactList.get(0))),
                () -> assertArrayEquals(contactService.findAll().toArray(),
                        adminContactList.stream()
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList())
                                .toArray()),
                () -> assertTrue(authService.logout()),

                () -> assertTrue(authService.login(authorizedVladimir)),
                () -> assertEquals(authService.getAuthUser().getLogin(), authorizedVladimir.getLogin()),

                () -> assertEquals((long) contactService.findAll().size(), 0),
                () -> vladimirContactList.forEach(contactService::save),
                () -> assertArrayEquals(contactService.findAll().toArray(),
                        vladimirContactList.stream()
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList())
                                .toArray()),
                () -> assertTrue(authService.logout()),

                () -> assertTrue(authService.login(authorizedAdmin)),
                () -> assertArrayEquals(contactService.findPhones().toArray(),
                        adminContactList.stream()
                                .filter(contact -> contact.getContactType() == ContactType.PHONE)
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList())
                                .toArray()),
                () -> assertArrayEquals(contactService.findEmails().toArray(),
                        adminContactList.stream()
                                .filter(contact -> contact.getContactType() == ContactType.EMAIL)
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList())
                                .toArray()),
                () -> assertArrayEquals(
                        contactService.findByName(adminContactList.get(0).getFullName().substring(4, 10)).toArray(),
                        adminContactList.stream()
                                .filter(contact -> contact.getFullName()
                                        .contains(adminContactList.get(0).getFullName().substring(4, 10)))
                                .collect(Collectors.toList()).toArray()),
                () -> assertArrayEquals(
                        contactService.findByValueStart("+380").toArray(),
                        adminContactList.stream()
                                .filter(contact -> contact.getValue().startsWith("+380"))
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList()).toArray()),
                () -> assertArrayEquals(
                        contactService.findByValueStart("medinets").toArray(),
                        adminContactList.stream()
                                .filter(contact -> contact.getValue().startsWith("medinets"))
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList()).toArray()),
                () -> assertTrue(contactService.deleteByValue(adminContactList.get(0).getValue())),
                () -> adminContactList.remove(adminContactList.get(0)),
                () -> assertArrayEquals(contactService.findAll().toArray(), adminContactList.toArray()),
                () -> assertTrue(authService.logout()),

                () -> assertTrue(authService.login(authorizedVladimir)),
                () -> assertArrayEquals(contactService.findPhones().toArray(),
                        vladimirContactList.stream()
                                .filter(contact -> contact.getContactType() == ContactType.PHONE)
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList())
                                .toArray()),
                () -> assertArrayEquals(contactService.findEmails().toArray(),
                        vladimirContactList.stream()
                                .filter(contact -> contact.getContactType() == ContactType.EMAIL)
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList())
                                .toArray()),
                () -> assertArrayEquals(
                        contactService.findByName(vladimirContactList.get(0).getFullName().substring(4, 10)).toArray(),
                        vladimirContactList.stream()
                                .filter(contact -> contact.getFullName()
                                        .contains(vladimirContactList.get(0).getFullName().substring(4, 10)))
                                .collect(Collectors.toList()).toArray()),
                () -> assertArrayEquals(
                        contactService.findByValueStart("+380").toArray(),
                        vladimirContactList.stream()
                                .filter(contact -> contact.getValue().startsWith("+380"))
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList()).toArray()),
                () -> assertArrayEquals(
                        contactService.findByValueStart("vladimir").toArray(),
                        vladimirContactList.stream()
                                .filter(contact -> contact.getValue().startsWith("vladimir"))
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList()).toArray()),
                () -> contactService.deleteByValue(vladimirContactList.get(0).getValue()),
                () -> vladimirContactList.remove(vladimirContactList.get(0)),
                () -> assertArrayEquals(
                        contactService.findAll().toArray(),
                        vladimirContactList.stream()
                                .sorted(Contact.byFullNameContactTypeComparator())
                                .collect(Collectors.toList()).toArray()),
                () -> assertTrue(authService.logout())
        );
    }

    private final Contact invalidatePhoneContact =
            new Contact()
                    .setFirstName("Invalidate")
                    .setLastName("Phone")
                    .setContactType(ContactType.PHONE)
                    .setValue("660538444");

    private final Contact invalidateEmailContact =
            new Contact()
                    .setFirstName("Invalidate")
                    .setLastName("Email")
                    .setContactType(ContactType.EMAIL)
                    .setValue("vladimir_kremena@gmail.com");

    private final List<Contact> adminContactList =
            new ArrayList<>(
                    List.of(
                            new Contact()
                                    .setFirstName("Ирина")
                                    .setLastName("Боброва")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380660599551"),
                            new Contact()
                                    .setFirstName("Ирина")
                                    .setLastName("Боброва")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("bobrova.ia@it.ua"),
                            new Contact()
                                    .setFirstName("Анна")
                                    .setLastName("Иванова")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380938117679"),
                            new Contact()
                                    .setFirstName("Анна")
                                    .setLastName("Иванова")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("ivanova.ak@it.ua"),
                            new Contact()
                                    .setFirstName("Александр")
                                    .setLastName("Мединец")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380943484625"),
                            new Contact()
                                    .setFirstName("Александр")
                                    .setLastName("Мединец")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("medinets.aa@it.ua"),
                            new Contact()
                                    .setFirstName("Никита")
                                    .setLastName("Плясов")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380979497286"),
                            new Contact()
                                    .setFirstName("Никита")
                                    .setLastName("Плясов")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("plyasov.nn@it.ua"),
                            new Contact()
                                    .setFirstName("Александр")
                                    .setLastName("Ткаченко")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380961322034"),
                            new Contact()
                                    .setFirstName("Александр")
                                    .setLastName("Ткаченко")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("tkachenco.aa@it.ua"),
                            new Contact()
                                    .setFirstName("Катерина")
                                    .setLastName("Шевченко")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380966140395"),
                            new Contact()
                                    .setFirstName("Катерина")
                                    .setLastName("Шевченко")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("shevchenko.km@it.ua")
                    )
            );

    List<Contact> vladimirContactList =
            new ArrayList<>(
                    List.of(
                            new Contact()
                                    .setFirstName("Владимир")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380660538444"),
                            new Contact()
                                    .setFirstName("Владимир")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("vladimirkremena@gmail.com"),
                            new Contact()
                                    .setFirstName("Виктор")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380955678865"),
                            new Contact()
                                    .setFirstName("Виктор")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("viktorkremena@gmail.com"),
                            new Contact()
                                    .setFirstName("Алла")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380501844530"),
                            new Contact()
                                    .setFirstName("Алла")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("kremenaalla@gmail.com"),
                            new Contact()
                                    .setFirstName("Мария")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.PHONE)
                                    .setValue("+380662849647"),
                            new Contact()
                                    .setFirstName("Мария")
                                    .setLastName("Кремена")
                                    .setContactType(ContactType.EMAIL)
                                    .setValue("mariyakremena93@gmail.com")
                    )
            );
}