package ua.kr.ex.acd.util.encoder.impl;

import ua.kr.ex.acd.util.encoder.PasswordEncoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5PasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(password.getBytes());
            byte[] digest = messageDigest.digest();
            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : digest){
                stringBuilder.append(String.format("%x", b));
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            throw  new RuntimeException(e);
        }
    }

    @Override
    public boolean verify(String password, String hash) {
        return hash.equals(encode(password));
    }
}