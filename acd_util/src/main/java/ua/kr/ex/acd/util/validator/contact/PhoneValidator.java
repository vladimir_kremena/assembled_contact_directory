package ua.kr.ex.acd.util.validator.contact;

import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.util.validator.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidator implements Validator<Contact> {

    private final static Pattern PHONE_PATTERN = Pattern.compile("(\\+380|380|80|0)(\\d{9})");

    @Override
    public boolean isValid(Contact contact) {
        if (contact.getContactType() != ContactType.PHONE) return true;
        return PHONE_PATTERN.matcher(contact.getValue()).matches();
    }

    public String getBody(Contact contact) {
        Matcher matcher = PHONE_PATTERN.matcher(contact.getValue());
        matcher.matches();
        return matcher.group(2);
    }
}