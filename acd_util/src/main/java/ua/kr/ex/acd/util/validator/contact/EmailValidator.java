package ua.kr.ex.acd.util.validator.contact;

import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.util.validator.Validator;

import java.util.regex.Pattern;

public class EmailValidator implements Validator<Contact> {

    private final static Pattern EMAIL_PATTERN = Pattern.compile("([0-9a-z.]+)@([0-9a-z]+\\.){1,2}[a-z]+");

    @Override
    public boolean isValid(Contact contact) {
        if (contact.getContactType() != ContactType.EMAIL) return true;
        return EMAIL_PATTERN.matcher(contact.getValue()).matches();
    }
}