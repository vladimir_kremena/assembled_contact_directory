package ua.kr.ex.acd.util.file;

import java.io.File;
import java.io.IOException;

public class FileUtil {

    public static void createDirectoryAndFile(File file) {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException iOE) {
                System.out.println("File creation error: " + iOE.getMessage());
            }
        }
    }
}
