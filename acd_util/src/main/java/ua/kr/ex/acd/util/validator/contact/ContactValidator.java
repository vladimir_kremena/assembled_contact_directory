package ua.kr.ex.acd.util.validator.contact;

import lombok.RequiredArgsConstructor;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.util.validator.Validator;

import java.util.List;

@RequiredArgsConstructor
public class ContactValidator implements Validator<Contact> {

    private final List<Validator<Contact>> validators;

    @Override
    public boolean isValid(Contact contact) {
        return validators.stream().allMatch(validator -> validator.isValid(contact));
    }
}