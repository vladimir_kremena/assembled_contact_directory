package ua.kr.ex.acd.util.validator;

public interface Validator<T> {

    boolean isValid(T t);

}