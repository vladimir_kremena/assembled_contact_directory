package ua.kr.ex.acd.util.property;

import java.io.FileReader;
import java.util.Properties;

public class ApplicationPropertiesReader {

    private static final String PROPERTY_SOURCE = "property.source";

    public static Properties getApplicationProperties() {
        Properties properties = new Properties();
        String pathToPropertySource = System.getProperty(PROPERTY_SOURCE);
        try {
            /* custom application config */
            properties.load(new FileReader(pathToPropertySource));
        } catch (Exception e) {
            e.printStackTrace();
            /* default application config */
            properties.setProperty("application.mode",  "CONSOLE_MODE");
            properties.setProperty("storage.mode",      "IN_MEMORY");
        }
        return properties;
    }
}