package ua.kr.ex.acd.util.encoder;

public interface PasswordEncoder {

    String encode(String password);

    boolean verify(String password, String hash);

}