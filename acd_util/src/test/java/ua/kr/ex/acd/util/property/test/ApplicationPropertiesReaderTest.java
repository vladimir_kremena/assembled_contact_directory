package ua.kr.ex.acd.util.property.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kr.ex.acd.util.property.ApplicationPropertiesReader;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ApplicationPropertiesReaderTest {

    @Test
    @DisplayName("application properties reader test")
    void applicationPropertiesReaderTest() {
        System.setProperty("property.source", "../config/custom_config.properties");
        Properties properties = ApplicationPropertiesReader.getApplicationProperties();
        assertAll(
                () -> assertNotNull(properties.getProperty("application.mode")),
                () -> assertNotNull(properties.getProperty("storage.mode"))
        );
    }
}