package ua.kr.ex.acd.util.validator.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kr.ex.acd.model.Contact;
import ua.kr.ex.acd.model.ContactType;
import ua.kr.ex.acd.util.validator.Validator;
import ua.kr.ex.acd.util.validator.contact.ContactValidator;
import ua.kr.ex.acd.util.validator.contact.EmailValidator;
import ua.kr.ex.acd.util.validator.contact.PhoneValidator;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ContactValidatorTest {

    @Test
    @DisplayName("contact validator test")
    void contactValidatorTest() {

        Contact validatePhoneContact =
                new Contact()
                        .setFirstName("Invalidate")
                        .setLastName("Phone")
                        .setContactType(ContactType.PHONE)
                        .setValue("0660538444");

        Contact validateEmailContact =
                new Contact()
                        .setFirstName("Invalidate")
                        .setLastName("Email")
                        .setContactType(ContactType.EMAIL)
                        .setValue("vladimirkremena@gmail.com");

        Contact invalidatePhoneContact =
                new Contact()
                        .setFirstName("Invalidate")
                        .setLastName("Phone")
                        .setContactType(ContactType.PHONE)
                        .setValue("660538444");

        Contact invalidateEmailContact =
                new Contact()
                        .setFirstName("Invalidate")
                        .setLastName("Email")
                        .setContactType(ContactType.EMAIL)
                        .setValue("vladimir_kremena@gmail.com");

        Validator<Contact> contactValidator = new ContactValidator(
                new ArrayList<>(
                        List.of(
                                new PhoneValidator(),
                                new EmailValidator()
                        )
        ));

        assertAll(
                () -> assertTrue(contactValidator.isValid(validatePhoneContact)),
                () -> assertTrue(contactValidator.isValid(validateEmailContact)),
                () -> assertFalse(contactValidator.isValid(invalidatePhoneContact)),
                () -> assertFalse(contactValidator.isValid(invalidateEmailContact))
        );
    }
}